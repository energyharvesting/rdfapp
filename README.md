rdfapp -- an angular.js router for applications that use RDF
============================================================

rdfapp is an [angular.js][] module that binds maps the fragment identifier
bidirectionally to a list of URIs (expressed as [AngulaRDF][] nodes), and
facilitates creating data driven user interface components by populating a
directive based on RDF metadata about those URIs.

This is work in progress; especially, it may become feasible to split its
functionality into a angular.js-router and a data-driven-directives component.

Implementation
==============

rdfapp itself consists of a single javascript file at `./modules/rdfapp/`,
which currently also houses further documentation.

Usage
=====

Source the main javascript file into your code, and add `rdfapp` to the list of
your application's required modules. Place an `rdfapp` attribute on one of the
outer elements (eg. on the same as your `angulardf` directive).

With that in place, in the scope of that directive, there is an `rdfapp` object
that provides methods for storing a resource in the fragment identifier (eg.
`.push()`, `.peek()`).

Beware that rdfapp completely takes over routing (and -- admittedly hackishly
-- in doing so rips out some of the regular routing mechanisms, because they
interfere with escaping inside the fragment identifier).

This can be conveniently explored in the [FoaF demo][]. (Note that the that
demo is not ideal to get an impression of what AngulaRDF can do, see its
shipped angulator demo for a better first start).


Furthermore, you can use the `<rdf-routed-view>` directive to transclude the
HTML sniplet given in your metadata as `A rdfapp:visualization ?y . ?y
rdfapp:body ?snip` where `A` is your page's address or the first working URI
stored in the fragment identifier.

An example to demonstrate that without too big an introduction is yet to be
added.

[angular.js]: http://angularjs.org/
[AngulaRDF]: https://gitlab.com/energyharvesting/angulardf
[FoaF demo]: ./foaf-demo.html
