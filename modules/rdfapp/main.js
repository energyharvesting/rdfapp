'use strict';
// vim:et:sw=4:ts=4:sts=4

/** Module that provides the rdfapp and rdfRoutedView directives, which allow
 * including HTML sniplets based on the current URL and an angulardf RDF graph.
 *
 * The rdfapp directive sets up an rdfapp variable on the scope (this could
 * arguably go into a service), and the rdfRoutedView directive does the actual
 * inclusion.
 *
 * The sniplet to include is determined by looking up a resource's
 * rdfapp:visualization; that visualization's rdfapp:body gets included
 * at the rdfRoutedView directive. The resource whose visualization is used is
 * stored on the scope as rdfapp.visualized_node. (The rdfapp namespace is
 * defined as <http://terms.energyharvesting.at/rdfapp#>).
 *
 * The resources for which visualizations are looked up are modelled as a list
 * whose first element is the current URL without the fragment identifier. This
 * allows for the visualizations to "fall back" -- when a node is viewed that
 * does not explicitly have a visualization set (and it is not desirable to
 * have those implementation notes spread over the user data), only the base
 * URL needs a visualization, which will then decide how to show the
 * lower-level node. (That by itself would only require a two-level system.)
 *
 * In order to display multiple visualizations at a user's discretion from a
 * single entry point (which use the scheme shown above), a three-level system
 * (http://my-thing/#visualization1;thing-visualized) would be needed. As
 * visualizations might want to work hierarchically themselves, the depth is
 * not limited.
 *
 * The complete list of components is stored in rdfapp.completepath in the form
 * of RDF nodes. The last item of the list that does have a valid visualization
 * is stored in rdfapp.visualized_node, and that visualization is used to fill
 * in the directive. In case the visualized node is not the first element of
 * the list, the visualization will probably want to ignore the "context" of
 * the earlier nodes in the list, but might offer an 'exit' button and popping
 * itself from the list.
 *
 * Do not manipulate rdfapp.completepath directly; instead, use the
 * rdfapp.push, .pop and other navigation convenience methods.
 *
 * Some usage models group several stack items into one user action (like a
 * database application immediately loading its default document; closing it
 * and thus "popping" the stack should exit the application). For those use
 * cases, the push_into_group and pop_group navigation methods can be used,
 * which keep track of what was pushed as a group. This tracking information is
 * not persisted in the URL, and some additional constraints on when to call
 * push_into_group might be added in the course of implementation.
 *
 *
 * Additionally, the object rdfapp.arguments stores strings in the URL
 * independent of the component list.
 *
 *
 * While not being part of the API, it is relevant how the URL stack is
 * serialized into the URL for external links, and thus documented:
 *
 * * All URLs but the first one are stored in the fragment identifier, which is
 *   for that purpose treated as a ';' separated list. If a '?' occurs in the
 *   fragment identifier, it and everything after it are ignored for that list.
 *
 * * Items in that list are stored in percent-encoding. As most legal URL
 *   characters are legal in the fragment identifier, the only characters that
 *   are required to be escaped are '#', ';', '?' and '%', others may be
 *   escaped. (Note that if a character is percent-escaped in the intended URL,
 *   it needs to be double-escaped in the fragment item list.) Empty items are
 *   ignored. (This is to allow passing arguments to a stack consisting only of
 *   the base URL).
 *
 *   Two complete addresses yield the same stack if (but not only if) the parts
 *   before the # are equal, and the lists resulting from splitting the
 *   fragment identifier and passing each part through decodeURIComponent are
 *   equal.
 *
 * * The fragment items are formed into complete URIs by iterating from the
 *   beginning of the list, initially starting with the current URL without the
 *   fragment identifier. If a part contains any of the characters '/', '#' or
 *   ':', it is treated as a relative URI and joined with the last URI. (If the
 *   part happens to be an absolute URI, that means that the resulting URI
 *   equals the part). Otherwise, it is treated as a fragment identifier, and
 *   the next URI is the last URI with the updated fragment identifier.
 *
 * * This scheme allows for a query string to be appended to the fragment
 *   identifier, as the '?' character will not show up there otherwise.
 *   Everything after the '?' is stored in rdfapp.arguments like a query
 *   string.
 *
 * For example:
 *
 * * http://ex.com/#foo: <http://ex.com/>, <http://ex.com/#foo>
 * * http://ex.com/#foo;bar: <http://ex.com/>, <http://ex.com/#foo>, <http://ex.com/#bar>
 * * http://ex.com/#%23foo;/%23bar: the same, just overly complex
 * * http://ex.com/#/card.ttl%23me: <http://ex.com/>, <http://ex.com/card.ttl#me>
 * * http://ex.com/#/card.ttl;me: <http://ex.com/>, <http://ex.com/card.ttl>, <http://ex.com/card.ttl#me>
 * * http://ex.com/#http://ex.de/card.ttl%23me: <http://ex.com/>, <http://ex.de/card.ttl#me>
 * * http://ex.com/directory/#./: <http://ex.com/directory/>, <http://ex.com/directory/>
 * * http://ex.com/directory/#../: <http://ex.com/directory/>, <http://ex.com/>
 *
 * Note that currently, the router relies on the RDF statements to be
 * preloaded, and will not try to load candidate URLs.
 */

angular.module('rdfapp', ['angulardf'])

.config(['$locationProvider', function($locationProvider) {
    /* the author would appreciate hints of how to tell angularjs to get its
     * hands off the URL in more polite words
     *
     * html5mode makes things better, but angular still insists on escaping
     * semicolons in the url. */
    $locationProvider.$get = ['$rootScope', '$browser', function($rootScope, $browser){
        var SimpleLocationMode = function() {};
        SimpleLocationMode.prototype.absUrl = function() { return $browser.url();};
        /* keep autoScrollWatch happy; we can't do what it wants to do anyway in this scheme */
        SimpleLocationMode.prototype.hash = function() { return ''; };

        /* this uses a proprietary extension because providing standard
         * interfaces takes the risk of other parts of angular using them and
         * messing with them. */
        SimpleLocationMode.prototype._set_hash = function(new_hash) { $browser.url('#' + new_hash); }

        var $location = new SimpleLocationMode()

        $browser.onUrlChange(function(newUrl) {
            /* minimally stripped-down of ng/location.js' version */
            if (!$rootScope.$$phase) $rootScope.$digest();
        });

        return $location;
    }];
}])

.directive('rdfapp', [
        '$location', '$compile',
function($location ,  $compile) {
    return {
        restrict: 'A',
        scope: true,
        controller: ['$scope', function($scope) {
            var ns = {
                'rdfapp': $rdf.Namespace('http://terms.energyharvesting.at/rdfapp#'),
                /* The project terms are accepted to ease migration; they are
                 * deprecated and will not be accepted in future versions. */
                'project': $rdf.Namespace('http://ns.energyharvesting.at/project#'),
            };

            $scope.rdfapp = {
                '_visualization_template': undefined,
                'visualized_node': undefined,
                'arguments': {},
                'completepath': [],
                'completepath_is_group': [],

                /** Fill completepath and arguments based on an updated URL */
                'dissect': function(url) {
                    var base, hash, query;

                    if (url.indexOf('#') == -1) {
                        hash = "";
                        base = url;
                    } else {
                        hash = url.substring(url.indexOf('#') + 1, url.length);
                        base = url.substring(0, url.indexOf('#'));
                    }

                    if (hash.indexOf('?') == -1) {
                        query = "";
                    } else {
                        query = hash.substring(hash.indexOf('?') + 1, hash.length);
                        hash = hash.substring(0, hash.indexOf('?'));
                    }

                    this.completepath = [$rdf.sym(base)];
                    this.completepath_is_group = [false];
                    var components = hash.split(';');
                    for (var i = 0; i < components.length; ++i)
                    {
                        if (components[i] === '') continue;
                        var decoded = decodeURIComponent(components[i]);
                        if (decoded.match(/^[^/#:]+$/))
                            decoded = '#' + decoded;
                        base = $rdf.Util.uri.join(decoded, base);
                        this.completepath.push($rdf.sym(base));
                        this.completepath_is_group.push(undefined);
                    }

                    this.arguments = {}
                    var querycomponents = query.split('&');
                    for (var i = 0; i < querycomponents.length; ++i)
                    {
                        if (querycomponents[i].length == 0) continue;
                        var key, value;
                        var eqindex = querycomponents[i].indexOf('=');
                        if (eqindex != -1) {
                            key = querycomponents[i].substring(0, eqindex);
                            value = querycomponents[i].substring(eqindex + 1, querycomponents[i].length);
                        } else {
                            key = querycomponents[i];
                            value = null;
                        }
                        this.arguments[key] = value;
                    }
                },

                /** Join the current arguments and completepath into a fragment
                 * identifier.  This assumes that the first component of the
                 * path is still the loaded URL. */
                'assemble': function() {
                    var lastpart = this.completepath[0];
                    var segments = [];

                    for (var i = 1; i < this.completepath.length; ++i) {
                        var base = lastpart.uri, next = this.completepath[i].uri;

                        /* refTo seems not to recognize cases where the path
                         * component is just '/' or does not end with a '/' as
                         * eligible to returning '#...'.
                         *
                         * (/bla/ to /bal/#x is #x, but /bla to /bla#x is
                         * /bla#x, and / to /#x is /#x instead of all being
                         * #x).
                         */

                        if (base.split('#')[0] == next.split('#')[0] && next.indexOf('#') != -1) {
                            var relurl = next.substring(next.indexOf('#'));
                        } else {
                            var relurl = $rdf.Util.uri.refTo(base, next);
                        }

                        if (relurl.match(/^#[^/#:]+$/)) relurl = relurl.substring(1);
                        else if (!relurl.match(/[:\/]/)) relurl = './' + relurl; /* otherwise it looks like a fragment and will not roundtrip */
                        if (relurl == '') {
                            /* empty is illegal, thus we need to be a little
                             * more verbose. we could simplify this by either
                             * placing the file name, './' or '/' in relurl,
                             * but it is correct and too much of a corner case
                             * to really care for beautiful URIs. */
                            relurl = next;
                        }
                        /* encodeURIcomponent would be legal, but we know that
                         * some strings are ok and prefer to not have them
                         * unnecessarily encoded */
                        segments.push(encodeURIComponent(relurl).replace(/%3A/g, ':').replace(/%2F/g, '/'));

                        lastpart = this.completepath[i];
                    }

                    var result = segments.join(';')

                    var querystring = [];

                    for (var key in this.arguments) {
                        if (this.arguments[key] === undefined) continue;
                        else if (this.arguments[key] === null) querystring.push(key);
                        else querystring.push(key + '=' + this.arguments[key]);
                    }

                    if (querystring.length) result = result + '?' + querystring.join('&');

                    return result;
                },

                /**
                 * Navigation convenience methods
                 *
                 * These methods allow applications to easily change the
                 * addresses on the stack.
                 *
                 * @{

                /** Append a node to the stack.
                 *
                 * It is expected that in most applications, this will be used
                 * for some kind of "going deeper".
                 * */
                'push': function(node) {
                    if (typeof node == "string") node = $rdf.sym(node);
                    this.completepath.push(node);
                    this.completepath_is_group.push(false);
                },

                /** Append a node to the stack, but grouped with the previous node
                 *
                 * Nodes pushed thusly will be popped as one when using pop_group().
                 */

                'push_into_group': function push_into_group(node) {
                    this.push(node);
                    this.completepath_is_group[this.completepath_is_group.length - 1] = true;
                },

                /** Remove the top object from the stack, returning it.
                 *
                 * This will always leave the root element in, as it can not be
                 * changed without leaving the application.
                 *
                 * It is expected that most applications will use this as an
                 * "up" mechanism, and discard the result.
                 * */
                'pop': function() {
                    if (this.completepath.length > 1) {
                        this.completepath_is_group.pop();
                        return this.completepath.pop();
                    }
                    return undefined;
                },

                /** Remove the top object(s) from the stack, returning them as list.
                 *
                 * This behaves like .pop(), but removes several items if
                 * grouping was used, and thus returns a list (which is usually
                 * one element long, longer in case of grouping, and empty if
                 * popping is impossible).
                 *
                 * It is expected that most applications will use this as an
                 * "up" mechanism, and discard the result.
                 */
                'pop_group': function pop_group() {
                    var result = [];
                    var popped_group;
                    while (this.completepath.length != 1) {
                        result.unshift(this.completepath.pop());
                        popped_group = this.completepath_is_group.pop();
                        if (popped_group !== true) break;
                    }
                    return result;
                },

                /** Indicate whether calling pop() would have any effect */
                'can_pop': function can_pop() {
                    return this.completepath.length > 1;
                },

                /** Indicate whether calling pop_group() would have any effect */
                'can_pop_group': function can_pop_group() {
                    var new_length = this.completepath.length;
                    while (new_length > 1 && this.completepath_is_group[new_length - 1]) new_length --;
                    return new_length > 1;
                },

                /** Return the top object from the stack without modifying anything */
                'peek': function() {
                    return this.completepath[this.completepath.length - 1];
                },

                /** Replace all stack elements under the current node with the
                 * given node or an Array thereof.
                 *
                 * Leaving out the argument is equivalent to go([]), which
                 * drops all stack elements under the visualized node.
                 * */
                'go': function(node) {
                    if (node === undefined) node = [];
                    if (!(node instanceof Array)) node = [node];
                    var newlength = this.completepath.indexOf(this.visualized_node) + 1;
                    if (newlength < 1) newlength = 1;
                    this.completepath.length = newlength;
                    this.completepath_is_group.length = this.completepath.length;
                    this.completepath = this.completepath.concat(node);
                    while (this.completepath_is_group.length != this.completepath.length) this.completepath_is_group.push(false);
                },

                /** Remove the visualized node and lower nodes from the stack,
                 * optionally replacing them with something else.
                 *
                 * This works in analogy to .go(), but also removes the
                 * visualized node. Note that it may do nothing at all if it
                 * can not remove the visualized node from the stack.
                 */
                'exit': function(node) {
                    if (node === undefined) node = [];
                    if (!(node instanceof Array)) node = [node];
                    var newlength = this.completepath.indexOf(this.visualized_node);
                    if (newlength < 1) newlength = 1;
                    this.completepath.length = newlength;
                    this.completepath_is_group.length = this.completepath.length;
                    this.completepath = this.completepath.concat(node);
                    while (this.completepath_is_group.length != this.completepath.length) this.completepath_is_group.push(false);
                }

                /** @} */
            };

            var rdfapp_debug = function(){};
            //rdfapp_debug = console.info;

            /** For each rdfapp state, there are multiple ways of expressing it
             * in a fragment identifier. This tracks what rdfapp would set if
             * it were to serialize the last state of the browser URL. As long
             * as what rdfapp would put in the address bar is equal to this,
             * there is no need to push it to the address bar again.
             *
             * This is primarily there to prevent rdfapp from rewriting input
             * URLs just because it would write them differently, but has the
             * nice side effect of deduplicating change events too. */
            $scope._last_input_canonicalization = undefined;
            /** Vice versa, this keeps dissect from being called after an assembly */
            $scope._last_generated_absurl = ":invalid:";

            $scope.$watch(function() { return $location.absUrl(); },
                function(abs) {
                    if (abs == $scope._last_generated_absurl) {
                        rdfapp_debug("URI changed to", abs, ", but no dissection needed.");
                        return;
                    }
                    if ($scope._last_generated_absurl.substr(0, 1) == "#" && abs.substr(abs.length - $scope._last_generated_absurl.length) == $scope._last_generated_absurl) {
                        /* to trigger this oddity, push from a first
                         * $scope.$watch expression, for example at the top of
                         * the visualized_node / _vizualization_template watch,
                         * and popping it later.
                         * */
                        rdfapp_debug("URI changed to", abs, ", ignoring bogus addition of protocol/netloc to what should always have been absolute.");
                        $scope._last_generated_absurl = abs;
                        return;
                    }
                    rdfapp_debug("URI changed to", abs, ", dissecting it.");
                    $scope.rdfapp.dissect(abs);
                    $scope._last_input_canonicalization = $scope.rdfapp.assemble();
                });

            $scope.$watch(function() { return $scope.rdfapp.assemble(); },
                function(newurl) {
                    if (newurl == $scope._last_input_canonicalization) {
                        rdfapp_debug("Stack changed to ", newurl, ", but no re-assembly needed.");
                        return;
                    }
                    rdfapp_debug("Stack changed to", newurl, ", assembling it.");
                    $location._set_hash(newurl);
                    $scope._last_generated_absurl = $location.absUrl();
                    $scope._last_input_canonicalization = newurl;
                });

            $scope.$watch(function() {
                for (var i = $scope.rdfapp.completepath.length - 1; i >= 0; i--) {
                    var visualizations = $scope.ardf.objects($scope.rdfapp.completepath[i], [ns.rdfapp('visualization'), ns.project('visualization')]);
                    var templates = $scope.ardf.objects(visualizations, [ns.rdfapp('body'), ns.project('rdfappbody')]);
                    if (templates.length == 0 || ! templates[0].uri) continue;

                    var template = templates[0];
                    $scope.rdfapp.visualized_node = $scope.rdfapp.completepath[i];
                    $scope.rdfapp._visualization_template = template.uri;

                    return;
                }
                $scope.rdfapp._visualization_template = undefined;
                $scope.rdfapp.visualized_node = undefined;
            });
        }]
    };
}])

.directive('rdfRoutedView', [
function() {
    return {
        restrict: 'E',
        template: '<ng-include src="rdfapp._visualization_template"></ng-include>',
    };
}])

/** Provide a function that allows directives with isolated scopes to
 * explicitly un-isolate the rdfapp variable.
 *
 * See angulardf's angulardfIsolatedScopeHack for rationale and usage.
 * */
.factory('rdfappIsolatedScopeHack', [function() {
    return function($scope) {
        $scope.rdfapp = $scope.$parent.rdfapp;
    }
}])

;
